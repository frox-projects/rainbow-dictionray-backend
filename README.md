6/10/2019:
Pipenv luotu kehitysympäristö. Kloonattua kaiken pitäisi toimia ihan hyvin, paitsi MYSQL. Credentiaalit mysql on paikallisesti /django_apps/backend/settings.py kohdassa DATABASE.


Sivu ajetaan päälle olemalla manage.py-tiedoston kansiossa kirjoittamalla "python manage.py runserver". Voit myös laittaa manage.py:lle täydellisen polun, jos haluat ympäristön päälle jossain muussa kansiossa.

Python version 3.7
Django 2.2.6 (FINAL)
mysql 14.14 distrib 5.7.27 for linux (tällä ei pitäisi olla merkitystä..?)
