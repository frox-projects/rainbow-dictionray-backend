"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from . import views

# todo: router or path?
#       views or viewsets?
#       decide endpoints and string format
#       Is name necessary?
# The patterns are tested starting from the top, first to match is called.
urlpatterns = [
    path('admin/', admin.site.urls),

    path('headword/', views.HeadwordListView.as_view(), name='headword-list'),
    path('headword/<pk>', views.SingleHeadwordView.as_view(), name='headword-detail'),
    path('choices/', views.ChoicesView.as_view(), name='choices'),
    path('definition/', views.DefinitionListView.as_view(), name='definition-list'),
    path('definition/<pk>', views.SingleDefinitionView.as_view(), name='definition-detail'),
    path('wordclass/', views.WordclassListView.as_view(), name='wordclass-list'),
    path('wordclass/<pk>', views.SingleWordclassView.as_view(), name='wordclass-detail'),
    path('application/', views.ApplicationListView.as_view(), name='application-list'),
    path('application/<pk>', views.SingleApplicationView.as_view(), name='application-detail'),
    path('style/', views.StyleListView.as_view(), name='style-list'),
    path('style/<pk>', views.SingleStyleView.as_view(), name='style-detail'),
    path('section/', views.SectionListView.as_view(), name='section-list'),
    path('section/<pk>', views.SingleSectionView.as_view(), name='section-detail'),
    path('occurrence/', views.OccurrenceListView.as_view(), name='occurrence-list'),
    path('occurrence/<pk>', views.SingleOccurrenceView.as_view(), name='occurrence-detail'),
    path('quotation/', views.QuotationListView.as_view(), name='quotation-list'),
    path('quotation/<pk>', views.SingleQuotationView.as_view(), name='quotation-detail'),
    # todo: provide CREATE VIEW statements if they are to be used
    # path('headwordstats/', views.HeadwordStatsListView.as_view(), name='headwordstats-list'),
    # path('headwordstats/<pk>', views.SingleHeadwordStatsView.as_view(), name='headwordstats-detail'),
    # path('headworddefinitions/', views.HeadwordDefinitionsListView.as_view(), name='headworddefinitions-list'),
    # path('headworddefinitions/<pk>', views.SingleHeadwordDefinitionsView.as_view(), name='headworddefinitions-detail'),
    # path('headwordquotations/', views.HeadwordQuotationsListView.as_view(), name='headwordquotations-list'),
    # path('headwordquotations/<pk>', views.SingleHeadwordQuotationsView.as_view(), name='he
]
