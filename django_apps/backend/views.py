# from django.shortcuts import render  # currently not used
from rest_framework import generics
from rest_framework import filters
from .models import (
    Headword, Definition, Wordclass, Application, Style,
    Section, Occurrence, Quotation,
    # HeadwordStats, HeadwordDefinitions, HeadwordQuotations
)
from .serializers import (
    HeadwordSerializer, DefinitionSerializer, WordclassSerializer,
    ApplicationSerializer, StyleSerializer, SectionSerializer,
    OccurrenceSerializer, QuotationSerializer,  # HeadwordStatsSerializer,
    # HeadwordDefinitionsSerializer, HeadwordQuotationsSerializer
)
# https://github.com/MattBroach/DjangoRestMultipleModels
from drf_multiple_model.views import ObjectMultipleModelAPIView


class HeadwordListView(generics.ListCreateAPIView):
    queryset = Headword.objects.all()
    serializer_class = HeadwordSerializer
    filter_backends = [filters.SearchFilter]
    search_fields = ['text']


class SingleHeadwordView(generics.RetrieveUpdateAPIView):
    queryset = Headword.objects.all()
    serializer_class = HeadwordSerializer


class DefinitionListView(generics.ListCreateAPIView):
    queryset = Definition.objects.all()
    serializer_class = DefinitionSerializer


class SingleDefinitionView(generics.RetrieveUpdateAPIView):
    queryset = Definition.objects.all()
    serializer_class = DefinitionSerializer


class WordclassListView(generics.ListCreateAPIView):
    queryset = Wordclass.objects.all()
    serializer_class = WordclassSerializer


class SingleWordclassView(generics.RetrieveUpdateAPIView):
    queryset = Wordclass.objects.all()
    serializer_class = WordclassSerializer


class ApplicationListView(generics.ListCreateAPIView):
    queryset = Application.objects.all()
    serializer_class = ApplicationSerializer


class SingleApplicationView(generics.RetrieveUpdateAPIView):
    queryset = Application.objects.all()
    serializer_class = ApplicationSerializer


class StyleListView(generics.ListCreateAPIView):
    queryset = Style.objects.all()
    serializer_class = StyleSerializer


class SingleStyleView(generics.RetrieveUpdateAPIView):
    queryset = Style.objects.all()
    serializer_class = StyleSerializer


class SectionListView(generics.ListCreateAPIView):
    queryset = Section.objects.all()
    serializer_class = SectionSerializer


class SingleSectionView(generics.RetrieveUpdateAPIView):
    queryset = Section.objects.all()
    serializer_class = SectionSerializer


class OccurrenceListView(generics.ListCreateAPIView):
    queryset = Occurrence.objects.all()
    serializer_class = OccurrenceSerializer


class SingleOccurrenceView(generics.RetrieveUpdateAPIView):
    queryset = Occurrence.objects.all()
    serializer_class = OccurrenceSerializer


class QuotationListView(generics.ListCreateAPIView):
    queryset = Quotation.objects.all()
    serializer_class = QuotationSerializer


class SingleQuotationView(generics.RetrieveUpdateAPIView):
    queryset = Quotation.objects.all()
    serializer_class = QuotationSerializer


class ChoicesView(ObjectMultipleModelAPIView):
    querylist = [
        {'queryset': Application.objects.all(),
         'serializer_class': ApplicationSerializer},
        {'queryset': Section.objects.all(),
         'serializer_class': SectionSerializer},
        {'queryset': Style.objects.all(),
         'serializer_class': StyleSerializer},
        {'queryset': Wordclass.objects.all(),
         'serializer_class': WordclassSerializer},
    ]
# todo: provide CREATE VIEW statements
# class HeadwordStatsListView(generics.ListAPIView):
#     queryset = HeadwordStats.objects.all()
#     serializer_class = HeadwordStatsSerializer
#
#
# class SingleHeadwordStatsView(generics.RetrieveAPIView):
#     queryset = HeadwordStats.objects.all()
#     serializer_class = HeadwordStatsSerializer
#
#
# class HeadwordDefinitionsListView(generics.ListAPIView):
#     queryset = HeadwordDefinitions.objects.all()
#     serializer_class = HeadwordDefinitionsSerializer
#
#
# class SingleHeadwordDefinitionsView(generics.RetrieveAPIView):
#     queryset = HeadwordDefinitions.objects.all()
#     serializer_class = HeadwordDefinitionsSerializer
#
#
# class HeadwordQuotationsListView(generics.ListAPIView):
#     queryset = HeadwordQuotations.objects.all()
#     serializer_class = HeadwordQuotationsSerializer
#
#
# class SingleHeadwordQuotationsView(generics.RetrieveAPIView):
#     queryset = HeadwordQuotations.objects.all()
#     serializer_class = HeadwordQuotationsSerializer
