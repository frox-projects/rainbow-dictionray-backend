# todo: For each model, consider if necessary to have:
#       docstring
#       xxxField verbose_name
#       ForeignKey related_name, at least for abstract classes
#       ForeignKey related_query_name (or not? It defaults to model name.)
#

from django.db import models
# from django.contrib.auth.models import User  # currently not used
# https://github.com/brechin/django-computed-property
from computed_property import ComputedCharField


# String of model name instead of Model name is used as the first
# argument of models.ForeignKey so they can be declared in any order.
class Headword(models.Model):
    text = models.CharField(max_length=100)
    extension = models.CharField(max_length=100, null=True, blank=True)
    new = models.BooleanField(default=True)
    wordclass = models.ForeignKey('Wordclass', on_delete=models.CASCADE)
    applications = models.ManyToManyField(
        'Application',
        through='HeadwordApplication',
        blank=True,
    )
    styles = models.ManyToManyField(
        'Style',
        through='HeadwordStyle',
        blank=True,
    )

    class Meta:
        unique_together = (('text', 'extension', 'wordclass'),)

    # todo: As the following are properties and not fields, they cannot
    #       be used in queries. If necessary, how could they be used as a
    #       search criteria from API?
    @property
    def earliest(self):
        """issue of earliest occurrence related to headword"""
        # occurrences refers to Occurrence.headword related_name
        qs = self.occurrences.aggregate(earliest=models.Min('issue'))
        return qs['earliest']

    @property
    def latest(self):
        """issue of latest occurrence related to headword"""
        qs = self.occurrences.aggregate(latest=models.Max('issue'))
        return qs['latest']

    # todo: Can below be improved, and how? Subqueries? SQL?
    @property
    def note(self):
        """ OR of .note:s in applications, styles, definitions and
        quotations related to headword"""
        # backend_headwordapplication_related refers to
        # HeadwordApplication.headword related_name
        qs_app = self.backend_headwordapplication_related.aggregate(
                note=models.Max('note')  # ORing boolean values
        )
        #  Set default, for if there are no applications related to headword.
        note_app = False
        if qs_app['note'] is not None:
            note_app = qs_app['note']

        qs_sty = self.backend_headwordstyle_related.aggregate(
            note=models.Max('note')
        )
        note_sty = False
        if qs_sty['note'] is not None:
            note_sty = qs_sty['note']

        qs_def = self.definitions.aggregate(
            note=models.Max('note')
        )
        note_def = False
        if qs_def['note'] is not None:
            note_def = qs_def['note']

        qs_occ = Occurrence.objects.filter(headword_id=self.id)
        note_quo = False
        for instance in qs_occ:
            qs_quo = instance.quotations.aggregate(
                note=models.Max('note')
            )
            if qs_quo['note']:
                note_quo = True

        return note_app | note_sty | note_def | note_quo

    def __str__(self):
        return self.text


class AbstractTextAbbr(models.Model):
    """abstract base class for Application, Section, Style, Wordclass"""
    text = models.CharField(max_length=100, unique=True)
    abbreviation = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return self.text

    class Meta:
        abstract = True


class Wordclass(AbstractTextAbbr):
    # override base parameter to provide verbose_name with space
    text = models.CharField('word class', max_length=100, unique=True)
    pass


class Application(AbstractTextAbbr):
    pass


class Style(AbstractTextAbbr):
    pass


class AbstractHeadwordM2MLabel(models.Model):
    """abstract base class for HeadwordApplication, -Style"""
    headword = models.ForeignKey(
        'Headword',
        on_delete=models.CASCADE,
        # each child Model must have unique argument values
        related_name='%(app_label)s_%(class)s_related',
        related_query_name='%(app_label)s_%(class)ss',
    )
    note = models.BooleanField(
        blank=True,
        null=False,
        default=False,
    )

    class Meta:
        abstract = True


class HeadwordApplication(AbstractHeadwordM2MLabel):
    application = models.ForeignKey('Application', on_delete=models.CASCADE)

    class Meta:
        db_table = 'backend_headword_application'
        unique_together = [['headword', 'application']]


class HeadwordStyle(AbstractHeadwordM2MLabel):
    style = models.ForeignKey('Style', on_delete=models.CASCADE)

    class Meta:
        db_table = 'backend_headword_style'
        unique_together = [['headword', 'style']]


class Definition(models.Model):
    headword = models.ForeignKey(
        'Headword',
        related_name='definitions',
        on_delete=models.CASCADE,
    )
    text = models.CharField(max_length=255)
    note = models.BooleanField(
        blank=True,
        null=True,
        default=False,
    )

    def __str__(self):
        return self.text


class Section(AbstractTextAbbr):
    pass


class Occurrence(models.Model):
    headword = models.ForeignKey(
        'Headword',
        related_name='occurrences',
        on_delete=models.CASCADE,
    )
    section = models.ForeignKey(
        'Section',
        related_name='sections',
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    source = models.CharField(max_length=100, default='Helsingin Sanomat')
    issue = models.DateField()
    page = models.SmallIntegerField()
    # todo: To be observed, if the following repeats:
    #       Change in date or page updates the reference in Django,
    #       but the value is not updated into database.
    # ComputedXxxField is read-only by default.
    reference = ComputedCharField(compute_from='hs_reference', max_length=20)

    @property
    def hs_reference(self):
        return 'HS' + self.issue.strftime('%Y%m%d') + str(self.page).zfill(3)

    def __str__(self):
        return 'HS' + self.issue.strftime('%Y%m%d') + str(self.page).zfill(3)


class Quotation(models.Model):
    occurrence = models.ForeignKey(
        Occurrence,
        related_name='quotations',
        on_delete=models.CASCADE
    )
    text = models.CharField(max_length=255)
    note = models.BooleanField(
        blank=True,
        null=True,
        default=False,
    )

    @property
    def issue(self):
        """reference to issue of quotation"""
        return Occurrence.objects.get(id=self.occurrence_id).issue

    def __str__(self):
        return self.text


class AbstractView(models.Model):
    """abstract base class for views defined in database"""
    headword = models.CharField('Headword', max_length=100, editable=False)
    hw_id = models.SmallIntegerField(editable=False, primary_key=True)

    class Meta:
        abstract = True
        managed = False


# # todo: provide CREATE VIEW statements if they are to be used
# class HeadwordStats(AbstractView):
#     """VIEW backend_hw_stats: collect the single-line data for each headword"""
#     wc = models.CharField(max_length=10, editable=False)
#     applications = models.TextField(editable=False)
#     styles = models.TextField(editable=False)
#     earliest = models.DateField(editable=False)
#     latest = models.DateField(editable=False)
#     sections = models.TextField(editable=False)
#     hw_note = models.BooleanField(editable=False)
#
#     class Meta(AbstractView.Meta):  # Inherit Meta from base class.
#         db_table = 'backend_hw_stats'
#
#     def __str__(self):
#         return self.headword
#
#
# class HeadwordDefinitions(AbstractView):
#     """VIEW backend_hw_definitions: collect the definitions for each headword"""
#     # override primary_key of base table to set it below
#     hw_id = models.SmallIntegerField(editable=False)
#     def_id = models.PositiveIntegerField(editable=False, primary_key=True)
#     definition = models.CharField(max_length=255, editable=False)
#
#     class Meta(AbstractView.Meta):
#         db_table = 'backend_hw_definitions'
#
#     def __str__(self):
#         return self.definition
#
#
# class HeadwordQuotations(AbstractView):
#     """VIEW backend_hw_occurrences: collect the quotations for each headword"""
#     # override primary_key of base table to set it below
#     hw_id = models.SmallIntegerField(editable=False)
#     quo_id = models.PositiveIntegerField(editable=False, primary_key=True)
#     quotation = models.CharField(max_length=255, editable=False)
#
#     class Meta(AbstractView.Meta):
#         db_table = 'backend_hw_quotations'
#
#     def __str__(self):
#         return self.quotation
