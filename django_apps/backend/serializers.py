# todo: filtering inputs
# todo: validating inputs
# todo: way to update models related to Headword via request to
#       /headword/<pk> URL (custom nested serializers?)
from rest_framework import serializers
# from django.db.models import OuterRef, Subquery  # currently not used
from .models import (
    Headword, Definition, Wordclass,
    Application, HeadwordApplication, Style, HeadwordStyle,
    Section, Occurrence, Quotation,
    # HeadwordStats, HeadwordDefinitions, HeadwordQuotations
)


class DefinitionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Definition
        fields = "__all__"


class WordclassSerializer(serializers.ModelSerializer):

    class Meta:
        model = Wordclass
        fields = "__all__"


class ApplicationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Application
        fields = "__all__"


class HeadwordApplicationSerializer(serializers.ModelSerializer):

    class Meta:
        model = HeadwordApplication
        fields = [
            'id',
            'note',
        ]


class StyleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Style
        fields = "__all__"


class HeadwordStyleSerializer(serializers.ModelSerializer):

    class Meta:
        model = HeadwordStyle
        fields = [
            'id',
            'note',
        ]


class SectionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Section
        fields = "__all__"


class QuotationSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    occurrence = serializers.PrimaryKeyRelatedField(
        queryset=Occurrence.objects.all(),
        many=False,
    )

    class Meta:
        model = Quotation
        fields = [
            'id',
            'occurrence',
            'text',
            'note',
            'issue',
        ]


class OccurrenceSerializer(serializers.ModelSerializer):
    source = serializers.CharField(read_only=True)
    headword = serializers.PrimaryKeyRelatedField(
        label='Headword',
        queryset=Headword.objects.all(),
        many=False,
    )
    section = serializers.PrimaryKeyRelatedField(
        queryset=Section.objects.all(),
        many=False,
    )
    reference = serializers.CharField(read_only=True)
    quotations = QuotationSerializer(many=True, read_only=True)

    class Meta:
        model = Occurrence
        fields = [
            'id',
            'source',
            'issue',
            'page',
            'headword',
            'section',
            'reference',
            'quotations',
        ]


class HeadwordSerializer(serializers.ModelSerializer):
    # todo: for wordclass, application, style (each now done differently):
    #       PrimaryKeyRelatedField, StringRelatedField or serializer?
    #       Different way for researcher views and browser views?
    wordclass = serializers.PrimaryKeyRelatedField(
        label='Word class',
        queryset=Wordclass.objects.all(),
        many=False,
    )
    applications = serializers.StringRelatedField(
        many=True,
    )
    # backend_headwordapplication_related refers to
    # HeadwordApplication.headword related_name
    application_notes = HeadwordApplicationSerializer(
        source='backend_headwordapplication_related',
        many=True,
    )
    styles = StyleSerializer(
        many=True,
    )
    style_notes = HeadwordStyleSerializer(
        source='backend_headwordstyle_related',
        many=True,
    )
    # todo: nested serializer for updating definitions via headword in API
    definitions = DefinitionSerializer(
        many=True,
    )
    # Occurrences are provided to provide quotations.
    occurrences = OccurrenceSerializer(
        many=True,
        read_only=True,
    )

    class Meta:
        model = Headword
        fields = [
            'id',
            'text',
            'extension',
            'new',
            'wordclass',
            'definitions',
            'applications',
            'application_notes',
            'styles',
            'style_notes',
            'earliest',
            'latest',
            'note',
            'occurrences',
        ]


# todo: provide CREATE VIEW statements if they are to be used
# # This refers to model with managed=false
# class HeadwordStatsSerializer(serializers.ModelSerializer):
#
#     class Meta:
#         model = HeadwordStats
#         fields = "__all__"
#     # todo: What fields to use, or all?
#     #       Same with other managed=false models.
#     # class HeadwordStatsSerializer(serializers.Serializer):
#     #     hw_id = serializers.IntegerField(read_only=True)
#     #     headword = serializers.CharField(read_only=True)
#     #     wc = serializers.CharField(read_only=True)
#     #     applications = serializers.CharField(read_only=True, style={'base_template': 'textarea.html'})
#     #     styles = serializers.CharField(read_only=True, style={'base_template': 'textarea.html'})
#     #     earliest = serializers.DateField(read_only=True)
#     #     latest = serializers.DateField(read_only=True)
#     #     sections = serializers.CharField(read_only=True, style={'base_template': 'textarea.html'})
#     #     hw_note = serializers.BooleanField(read_only=True)
#
#
# class HeadwordDefinitionsSerializer(serializers.ModelSerializer):
#
#     class Meta:
#         model = HeadwordDefinitions
#         fields = "__all__"
#
#
# class HeadwordQuotationsSerializer(serializers.ModelSerializer):
#
#     class Meta:
#         model = HeadwordQuotations
#         fields = "__all__"
